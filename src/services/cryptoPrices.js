import { getCryptoPrice } from '../api/cryptoPrice.api'

const cryptoPrice = async ({ from, to }) =>
  await getCryptoPrice(from, to)
    .then((response) => response.data)
    .catch((err) => console.log(err))

export { cryptoPrice }
