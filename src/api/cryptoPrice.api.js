import { request } from './generic.api'

const getCryptoPrice = (from, to) =>
  request({
    method: 'post',
    url: `api/calculator/exchange/calculate`,
    data: { currency_from: from, currency_to: to },
    headers: { Accept: 'application/json' }
  })

export { getCryptoPrice }
