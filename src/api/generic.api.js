import axios from 'axios'

export const request = async ({ url, method = 'get', headers = {}, params = {}, data = {} }) => {
  const response = await axios({
    url: `${import.meta.env.VITE_BASE_URL}${url}`,
    method,
    headers,
    data
  })
  return response.data
}
