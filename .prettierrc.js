module.exports = {
  semi: false,
  tabWidth: 2,
  useTabs: false,
  printWidth: 150,
  endOfLine: 'auto',
  singleQuote: true,
  trailingComma: 'none',
  bracketSpacing: true,
  arrowParens: 'always',
  vueIndentScriptAndStyle: true
}
